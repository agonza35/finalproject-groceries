# FinalProject-Groceries

Project Name: MyVirtual Grocery List

Project by Ana Sofia Gonzalez (agonza35) & Sofia Lombardo (slombard)

Description of Data Source: 

Data Source: https://github.com/drriley/grocery/blob/master/lib/items.csv

Our data (linked above) is in a csv format, where every line has features of a different grocery store item, delimited by commas. Each item has a name, brand, category, and location. For the purposes of our project, we added two additional features for each item. We added an item ID number and the item price.

The user may find this data above useful because it would aid them in their decision as to what they would want to add, remove change or update from their virtual grocery list. For example, a user would be able to see details such as an item's brand and location - two categories that are essential for helping the user see where they can find/place that grocery item (in their fridge, pantry. freezer) or decide what brand of item want to add to their grocery list. In addition, the price data would help users calculate the final total of their virtual grocery list depending on what items they have added. 

## OO API

API Usage:  Our API, groceries_library.py, has a number of methods that allow users to load, access, add, and delete grocery items from a database. When a user makes a request, the API is called upon by a controller to retrieve data from the database and issue a response. The functions within the API are listed below.


OO API Functions / Features: 
_groceries_database primary class - initializes a dictionary for each data cateogry in the csv file (ex: item, brand, category, location and price) and contains the definitions for the methods / functions as described below

    * load_groceries: if passed a file (in this case the groceries.dat file), it will open that file and parse it for each data component / attribute we are looking for. Each line / item has an itemID which will be used as the index for each component's dictionary. For every itemID, the item, brand, category, location and price dictionaries are populated with the information for that item with that specific id (ex: self.groceries_brand[itemID] = brand)
    
    * get_groceries: this function returns the itemID of every grocery item in the current database

    * get_grocery: if passed an itemID, this function will return a list of all of the associated item's details, including name, brand, category, location, and price
    
    * set_grocery: if passed an itemID and list of item attributes, this function will add a new grocery item to the database
    
    * delete_grocery: if passed an itemID, this function will delete the entry associated with this ID, along with all of the items details


How to Run Test: 

    (1) test_api.py

        python3 test_api.py --> tests load_groceries, get_groceries, get_grocery, set_grocery, and delete_grocery

## REST API Specification

Port Number: 51021
Webservice Usage: The server provides information about grocery store items, including name, brand, food category, storing location, and price. This infomration can be accessed by users who are frequency grocery shoppers who wish to keep track of purchases, spending budgets, item details, or storing information. There are four different types of requests that can be made to the server, including GET, PUT, POST, and DELETE. Users can utilize these different types of requests to access information about grocery items, create a new shopping list, update item information, or add/delete items. There is a link below to our RESTful JSON API Specification that outlines these requests, as well as the resources and responses, in more detail.

RESTful API Specification Table: https://docs.google.com/document/d/17EzvhdIPDZ77w3LsqK-gIOhvqMTM83wj_N-J0XAuHP0/edit?usp=sharing 

How to run tests:


    (1) test_grocery_key.py --> tests PUT_KEY, GET_KEY, DELETE_KEY for /groceries/ resource

        python3 test_groceries_key.py

    (2) test_grocery_index.py --> tests GET_INDEX, POST_INDEX, DELETE_INDEX for /groceries/ resource

        python3 test_grocery_index.py

    (3) test_reset_endpoint.py --> tests PUT_INDEX, PUT_KEY for /reset/ resource

        python3 test_reset_endpoint.py
    
## User Interaction 
Steps for running entire project code:


    (1) Start the server
        --> cd into the server directory
        --> once inside the server directory/folder, run 'python3 server.py'

    (2) Open Webpage (Hosted Locally)
        --> cd into the frontend directory
        --> once inside the frontend directory/folder, run 'open index.html'

        
    (3) Utilize Web Client
        --> click 'See Grocery Stock' button to view entire grocery inventory
        --> click 'Create MyVirtual List' button to view the shopping list section
        --> enter item IDs into the text box and click the 'add' button
        --> scroll down to see the additions to the virtual shopping list and total price
        --> click the 'clear' button to clear the shopping list
    
        


## Presentation Slides Link
https://docs.google.com/presentation/d/1AwLeeaX6KEBUEsa1SRGurzvo_YF3kiHanwNKBMdRCm4/edit?usp=sharing


## Gitlab Repo Link: 
https://gitlab.com/agonza35/finalproject-groceries


## Zip File Link: 
https://drive.google.com/file/d/1danD9J0mnfGbHfZT-9FeyKzrJpU8sOYu/view?usp=sharing


## Videos Link 
Demo:
https://drive.google.com/file/d/1S5hxkLiUX0CGDwSgDJwL19SKNobBaqp9/view?usp=sharing

Code Walkthrough:
https://drive.google.com/file/d/1tKkM71LhKT96jaEeTVNyrOAYKsGT2-nY/view?usp=sharing



## Complexity
For this project, the complexity lies in the fact that we combined our frontend with our server and OO API. For the frontend we dynamically added elements/content to our page, as well as used a bit of CSS for added styling. We felt this added to our UI visual experience and exceeded our original sketch.



