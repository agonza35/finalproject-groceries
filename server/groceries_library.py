class _groceries_database:
    def __init__(self):
        self.groceries_item = dict()
        self.groceries_brand = dict()
        self.groceries_category = dict()
        self.groceries_location = dict()
        self.groceries_price = dict()
    
    def load_groceries(self, groceries_file):
        f = open(groceries_file)
        
        for line in f:
            line = line.rstrip()
            components = line.split(",")
            itemID = int(components[0])
            item = components[1]
            brand = components[2]
            category = components[3]
            location = components[4]
            price = components[5]
            self.groceries_item[itemID] = item
            self.groceries_brand[itemID] = brand
            self.groceries_category[itemID] = category
            self.groceries_location[itemID] = location
            self.groceries_price[itemID] = price
        f.close()
    
    def get_groceries(self):
       return self.groceries_item.keys()
        
    def get_grocery(self, itemID):
        try:
            item = self.groceries_item[itemID]
            brand = self.groceries_brand[itemID]
            category = self.groceries_category[itemID]
            location = self.groceries_location[itemID]
            price = self.groceries_price[itemID]
            grocery_item = list((item, brand, category, location, price))
        except Exception as ex:
            grocery_item = None
        return grocery_item
        
    def set_grocery(self, itemID, grocery_item):
        self.groceries_item[itemID] = grocery_item[0]
        self.groceries_brand[itemID] = grocery_item[1]
        self.groceries_category[itemID] = grocery_item[2]
        self.groceries_location[itemID] = grocery_item[3]
        self.groceries_price[itemID] = grocery_item[4]
        
      
    
    def delete_grocery(self, itemID):
        del(self.groceries_item[itemID])
        del(self.groceries_brand[itemID])
        del(self.groceries_category[itemID])
        del(self.groceries_location[itemID])  
        del(self.groceries_price[itemID])  
        
if __name__ == "__main__":
    gdb = _groceries_database()
    
    print('test')
    print(gdb.load_groceries('groceries.dat'))
    
    print(gdb.get_groceries())

    grocery = gdb.get_grocery(2)
    print(grocery)
    print(grocery[0])
    
    grocery[0] ='ABC'
    gdb.set_grocery(2, grocery)
    
    grocery = gdb.get_grocery(2)
    print(grocery[0])
    print(grocery)
    
    gdb.delete_grocery(2)
    print(gdb.get_groceries())
    
    
    