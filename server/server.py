#this file starts up the server and connects request/event types with event handlers
import routes
import cherrypy
from groceries_controller import GroceriesController #getting our class
from reset_controller import ResetController
from groceries_library import _groceries_database


#TODO

def start_service():
    

    #dispatcher
    dispatcher = cherrypy.dispatch.RoutesDispatcher()
    
    gdb = _groceries_database()
    
    #objects for Controllers
    groceriesController = GroceriesController(gdb=gdb)
    resetController = ResetController(gdb=gdb)
    

    #use dispatcher to connect resources to event handlers
    #connect(out_tag, http resource, class object with handler, event handler name, what type of HTTP request to serve)
    
    #get_index
    dispatcher.connect('groceries_get_all','/groceries/', controller=groceriesController, action='GET_INDEX', conditions=dict(method=['GET']))
    
    #get_key
    dispatcher.connect('groceries_get_key','/groceries/:itemID', controller=groceriesController, action='GET_KEY', conditions=dict(method=['GET']))
    
    #put_key
    dispatcher.connect('groceries_put_key','/groceries/:itemID', controller=groceriesController, action='PUT_KEY', conditions=dict(method=['PUT']))
   
    #post_index
    dispatcher.connect('groceries_post_new','/groceries/', controller=groceriesController, action='POST_INDEX', conditions=dict(method=['POST']))
    
    #delete_key
    dispatcher.connect('groceries_delete_key','/groceries/:itemID', controller=groceriesController, action='DELETE_KEY', conditions=dict(method=['DELETE']))
    
    #delete_index
    dispatcher.connect('groceries_delete_all','/groceries/', controller=groceriesController, action='DELETE_INDEX', conditions=dict(method=['DELETE']))
    
    #reset put_key
    dispatcher.connect('reset_put', '/reset/:itemID', controller=resetController, action = 'PUT_KEY', conditions=dict(method=['PUT']))
    
    #reset put_index
    dispatcher.connect('reset_index_put', '/reset/', controller=resetController, action = 'PUT_INDEX', conditions=dict(method=['PUT']))


    # default OPTIONS handler for CORS, all direct to the same place
    dispatcher.connect('groceries_options', '/groceries/', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))
    dispatcher.connect('groceries_key_options', '/groceries/:key', controller=optionsController, action='OPTIONS', conditions=dict(method=['OPTIONS']))

    #set up configuration
    conf = {
        'global' : {
            'server.socket_host' : 'localhost', #'student04.cse.nd.edu',
            'server.socket_port' : 51021,
            },
        '/' : {
            'request.dispatch' : dispatcher,
            'tools.CORS.on' : True, # configuration for CORS
            }
    }

    #update with new configuration
    cherrypy.config.update(conf)
    app = cherrypy.tree.mount(None, config=conf) # create app
    cherrypy.quickstart(app)    # start app

# class for CORS

class optionsController:
    def OPTIONS(self, *args, **kwargs):
        return ""



# function for CORS

def CORS():
    cherrypy.response.headers["Access-Control-Allow-Origin"] = "*"
    cherrypy.response.headers["Access-Control-Allow-Methods"] = "GET, PUT, POST, DELETE, OPTIONS"
    cherrypy.response.headers["Access-Control-Allow-Credentials"] = "true"




if __name__ == '__main__':
    cherrypy.tools.CORS = cherrypy.Tool('before_finalize', CORS) # CORS
    start_service()


