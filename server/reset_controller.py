import cherrypy
import re, json
from groceries_library import _groceries_database


class ResetController(object):
    def __init__(self, gdb=None):
        if gdb is None:
            self.gdb = _groceries_database()
        else: 
            self.gdb = gdb
            
    # PUT INDEX
    def PUT_INDEX(self):
        output = {'result' : 'success'}
        
        data = json.loads(cherrypy.request.body.read().decode())
        
        self.gdb.__init__()
        self.gdb.load_groceries('groceries.dat')
        
        return json.dumps(output)


    # PUT KEY
    def PUT_KEY(self, itemID):
        output = {'result': 'success'}
        itemID = int(itemID)
        
        try:
            data = json.loads(cherrypy.request.body.read().decode())
            
            gdbtmp = _groceries_database()
            gdbtmp.load_groceries('groceries.dat')
            
            grocery = gdbtmp.get_grocery(itemID)
            self.gdb.set_grocery(itemID, grocery)
        except Exception as ex:
            output['result'] = 'error'
            output['message'] = str(ex)
            
        return json.dumps(output)
    