import unittest
import groceries_library

class TestApiMethods(unittest.TestCase):
    

    def test_load(self):
        test_gdb = groceries_library._groceries_database()
        test_gdb.load_groceries('groceries.dat')
        
        self.assertIsNotNone(test_gdb.groceries_item[0]) 
        self.assertIsNotNone(test_gdb.groceries_brand[0]) 
        self.assertIsNotNone(test_gdb.groceries_category[0])
        self.assertIsNotNone(test_gdb.groceries_location[0])
        self.assertIsNotNone(test_gdb.groceries_price[0])
        
        
    def test_get_groceries(self):
        test_gdb = groceries_library._groceries_database()
        test_gdb.load_groceries('groceries.dat')
        self.assertEqual(len(test_gdb.get_groceries()), 34) 

        
    def test_get_grocery(self):
        test_gdb = groceries_library._groceries_database()
        test_gdb.load_groceries('groceries.dat')
        itemID = 4
        self.assertEqual(test_gdb.get_grocery(itemID), ['Chocolate Ice Cream', 'Breyer’s Chocolate Ice Cream', 'Sweets', 'Freezer' ,'4.29'])
    
    def test_set_grocery(self):
        test_gdb = groceries_library._groceries_database()
        test_gdb.load_groceries('groceries.dat')
        
        
        grocery = test_gdb.get_grocery(2)
        grocery[0] ='New Item'
        test_gdb.set_grocery(2, grocery)
        
        self.assertEqual(test_gdb.get_grocery(2)[0], 'New Item')
        
        
    def test_delete_grocery(self):
        test_gdb = groceries_library._groceries_database()
        test_gdb.load_groceries('groceries.dat')
        itemID = 7
        test_gdb.delete_grocery(itemID)
        self.assertFalse(test_gdb.get_grocery(itemID))
    
if __name__ == "__main__":
    unittest.main()
