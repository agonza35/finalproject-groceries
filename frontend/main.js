console.log('page load - enter main.js');


// http://localhost:51021/groceries/


// Dictionary for total price 
listTotal = {};

loadAllData();

function loadAllData() {
    console.log('inside load data')

    var url = 'http://localhost:51021/groceries/'
    var xhr = new XMLHttpRequest()
    
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        console.log('network response recieved' + xhr.responseText);
        parseData(xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); 

}

function parseData(response_text){
    // TODO : parse all response data
    var json_data = JSON.parse(response_text);

    var list_itemID = "";
    var list_item = "";
    var list_brand = "";
    var list_category = "";
    var list_location = "";
    var list_price = "";
    
    var items = json_data.items;
    for (var i = 0; i < items.length; i++){
        list_itemID = items[i].itemID;
        list_item = items[i].item;
        list_brand = items[i].brand;
        list_category = items[i].category;
        list_location = items[i].location;
        list_price = items[i].price;
        console.log(list_itemID, list_item, list_brand, list_category, list_location, list_price);

        addRow(list_itemID, list_item, list_brand, list_category, list_location,list_price);

    } //end of for loop 
       
} //end of parse data function 


// take the 5 elements to add
function addRow(itemID, item, brand, category, location, price) {
    
    var tbodyRef = document.getElementById('table-rows');
    
    // Insert a row at the end of table
    var newRow = tbodyRef.insertRow();

    // Insert a cell at the end of the row
    var newCell1 = newRow.insertCell();
    var newCell2 = newRow.insertCell();
    var newCell3 = newRow.insertCell();
    var newCell4 = newRow.insertCell();
    var newCell5 = newRow.insertCell();
    var newCell6 = newRow.insertCell();
   
    // Append text nodes to the cell
    var newText1 = document.createTextNode(itemID);
    newCell1.appendChild(newText1);

    var newText2 = document.createTextNode(item);
    newCell2.appendChild(newText2);

    var newText3 = document.createTextNode(brand);
    newCell3.appendChild(newText3);
    
    var newText4 = document.createTextNode(category);
    newCell4.appendChild(newText4);

    var newText5 = document.createTextNode(location);
    newCell5.appendChild(newText5);

    var newText6 = document.createTextNode(price);
    newCell6.appendChild(newText6);
    
} // end of addRow

var added = document.getElementById('add-button'); // link to submit button
added.onmouseup = onClickAdderResponse;

function onClickAdderResponse(){
    var itemID = document.getElementById('inputID-form').value; 
    make_request(itemID);
}

function make_request(itemID){
    console.log('inside make_request')
    var url = 'http://localhost:51021/groceries/' + itemID
    var xhr = new XMLHttpRequest()
    
    xhr.open("GET", url, true);

    xhr.onload = function(e){
        console.log('network response recieved' + xhr.responseText);
        parseShoppingData(xhr.responseText);
    }

    xhr.onerror = function(e){
        console.error(xhr.statusText);
    }

    xhr.send(null); 
}

function parseShoppingData(response_text){
    var shoppingData = JSON.parse(response_text);
    // if ID is out of range 
    if (shoppingData.result == 'error') {
        const input_box = document.getElementById('inputID-form');
        input_box.value = '';
        input_box.placeholder = 'Error: Enter valid itemID'
    } else {
        var list_itemID = shoppingData.itemID;
        var list_item = shoppingData.item;
        var list_price = shoppingData.price;

        var input_box = document.getElementById('inputID-form');
        input_box.placeholder = 'Enter itemID';
    
        console.log(list_itemID, list_item, list_price);
        addToShoppingList(list_itemID, list_item, list_price);
    }
}

function addToShoppingList(itemID, item, price){
    var tbodyRef = document.getElementById('table2-rows');
    
    // Insert a row at the end of table
    var newRow = tbodyRef.insertRow();

    // Insert a cell at the end of the row
    var newCell1 = newRow.insertCell();
    var newCell2 = newRow.insertCell();
    var newCell3 = newRow.insertCell();
   
    // Append text nodes to the cell
    var newText1 = document.createTextNode(itemID);
    newCell1.appendChild(newText1);

    var newText2 = document.createTextNode(item);
    newCell2.appendChild(newText2);

    var newText3 = document.createTextNode(price);
    newCell3.appendChild(newText3);

    listTotal[itemID] = price; 
    calculateTotalPrice();

}

function calculateTotalPrice(){
    var total = 0;

    for (const[key, value] of Object.entries(listTotal)){
        total +=  parseFloat(value);
    }
    console.log(total)
    var price = document.getElementById('price-icon');
    price.innerHTML = 'MyList Total: $' + total.toFixed(2);
    
}

var cleared = document.getElementById('clear-button'); // link to clear button
cleared.onmouseup = onClickClearResponse;

function onClickClearResponse(){
    var Table = document.getElementById('table2-rows');
    Table.innerHTML = '';

    var clear_price = document.getElementById('price-icon');
    clear_price.innerHTML = 'MyList Total: $';

    //clears dictionary
    listTotal = {};
   
    console.log('cleared dictionary')
}


